Lighthouse : 
Performance : 72
Accessibility : 66
Best Practices : 100
SEO : 50

W3C validator : 
9 erreurs :
Noms de fichiers à revoir "Kirisame.Marisa.full.1250399 - Copie.jpg", les espaces ne sont pas autorisés. Nom de fichier trop long aussi à mon avis. 
- Le problème des espaces dans les noms de fichiers est récurrent. 
- Penser aux "alt" pour les malvoyants dans la description des images. 
- scrolling="yes" frameborder="no" : Obsolète, utiliser le CSS à la place.

Test responsive :
Lorsque l'on passe à un autre format, le texte n'est plus centré et les images ont une taille peut être un peu trop importante vis à vis du reste. + Footer déréglé.

Contenu : 
Au niveau du contenu, personnellement j'aime beaucoup. La seule question que je me pose c'est de savoir si un recruteur prendra le temps de lire tes textes. C'est intéressant et ça nous permet d'en apprendre plus sur toi c'est vrai, mais à côté de ça je me demande si regrouper tes compétences dans une catégorie plus "claire", imagée, ne serait pas une bonne idée. Mais encore une fois c'est une question de temps de lecture pour un recruteur. A côté de ça tes textes sont relativement court et en plus de ça tu as les mots clés en gras, ce qui compense. Je trouve ton cv super :). 

Pourquoi ces couleurs ? (Je te pose la question parce-qu'on me l'a posé aussi en entretien). 

En bref, ton cv permet rapidement de cerner ton côté artistique, et j'aime bien la touche d'humour ! 
