Lighthouse : 
Performance : 86%
Accessibility : 100%
Best Practices : 100%
SEO : 58%

W3C validator : 
Seulement deux erreurs, dont une qui concerne google fonts, donc : Bravo Mathieu ! 

Test responsive :
Pas du tout responsive au niveau du header. Le reste (que du texte) l'est. 

Contenu : 
L'identité de la personne n'est pas forcément la chose qui devrait sauter aux yeux à l'ouverture du site. Le côté pixels est sympa mais la police d'écriture est peut-être trop grande, surtout par rapport au reste du CV avec une typo Disney sympa, mais disproportionnelle vis-à-vis du reste. 
Je placerais les compétences plus haut sur la page et j'agrandirais les sous-titres pour les rendre un peu plus lisibles. 

Ranger les compétences en fonction du métier visé. Ici peut être mettre en avant tes atouts en numérique, l'écriture, la sécurité... 

Aucun moyen pour te contacter ? 