
    $(document).ready(function(){
        // Ajout de scrollspy à <body>
        $('body').scrollspy({target: ".navbar", offset: 50});   
    
        // Ajout de smooth scrolling dans tous les liens de la navbar
        $("#myNavbar a").on('click', function(event) {
        // S'assurer que this.hash a une valeur avant de lui assigner une valeur par default
        if (this.hash !== "") {
            // Empêcher le comportement de clic d'ancrage par défaut
            event.preventDefault();
    
            // Store hash
            var hash = this.hash;
    
            // Utilise jQuery's animate() methode pour ajout du smooth page scroll
            // (800) est optionnel, il specifie le nombre de milliseconds que prend le scroll pour naviguer d'un point à l'autre
            $('html, body').animate({
            scrollTop: $(hash).offset().top
            }, 800, function(){
        
            // Ajout du hash (#) à l'URL quand le scrolling est terminé
            window.location.hash = hash;
            });
        }  // End if
        });
    });

    $(function () {
    
    $('#contact-form').submit(function(e) {
        e.preventDefault();
        $('.comments').empty();
        var postdata = $('#contact-form').serialize();
        
        $.ajax({
            type: 'POST',
            url: 'php/contact.php',
            data: postdata,
            dataType: 'json',
            success: function(json) {
                 
                if(json.isSuccess) 
                {
                    $('#contact-form').append("<p class='thank-you'>Votre message a bien été envoyé. Merci de m'avoir contacté. Je vous répondrais dans les plus brefs délais !</p>");
                    $('#contact-form')[0].reset();
                }
                else
                {
                    $('#firstname + .comments').html(json.firstnameError);
                    $('#name + .comments').html(json.nameError);
                    $('#email + .comments').html(json.emailError);
                    $('#phone + .comments').html(json.phoneError);
                    $('#message + .comments').html(json.messageError);
                }                
            }
        });
    });

})