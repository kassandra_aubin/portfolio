Journée du numérique à Saint-Malo le 24 octobre 2019 : 

Pour une première, j'ai trouvé l'évènement très intéressant. 

Dans un premier temps : 

Dès mon arrivée le matin je me suis rendu à la CCI pour effectuer des tests utilisateurs sur différents sites web. J'y ai notamment rencontré le patron de Happy PC 35, qui était sur la construction d'un site à destination des Malouins afin d'y regrouper les divers événements et les différentes possibilités / activitées à effectuer sur le secteur de Saint-Malo. Présent avec son développeur et prévenu que la veille au soir pour la tenue de l'événement, il m'a informé que son site n'était pas terminé et nous avons échangé sur les possibles modifications à effectuer pour le rendre plus pertinent en matière de cible, mais aussi de contenu. 

Point positif : Cette expérience était enrichissante aussi bien de notre côté que du leur finalement puisque ça leur permettait d'avoir des idées nouvelles, d'un regard neuf, sur leurs outils numériques et ça nous permettait d'avoir quelques contacts pour la suite. 

Point négatif : L'organisation. Le matin en tout cas, il y avait énormément de monde et ce n'était pas forcément très bien agencé. Les tests utilisateurs ne devaient durer que 10 à 15 minutes, mais ce n'était pas le cas. Il aurait été pratique d'avoir un chrono permettant de savoir combien de temps il restait pour ceux nous précédant afin de se positionner plus rapidement aux différents stands et ainsi gagner en efficacité. 

Dans un second temps : 

Suite à mon aventure à la CCI, je me suis rendu au Poool, à côté de la médiathèque. J'y ai fais la rencontre de trois boîtes dont deux ont retenues mon attention. 

- Une start-up : Senext, qui oeuvre pour que les jeunes retraités souhaitant rester en activité puissent être, en fonction de leurs compétences, missionnés par période au sein d'entreprises qui en ont besoin. La présidente de Senext m'a laissé sa carte et je l'ai relancée vendredi soir via LinkedIn. Le fait que j'ai travaillé plusieurs mois à la Carsat et mon expérience au sein d'Insaniam, dont elle connaît le président, ont joués en ma faveur sur le moment. Elle était intéressée par mon profil, j'attends son retour. 

- Le Groupe Beaumanoir : Avant de faire leur rencontre je m'étais déjà renseignée sur eux. L'idée d'évoluer humainement et professionnellement au sein d'un grand groupe m'intéressait. Cette idée s'est confortée à leur rencontre. J'ai pu expliquer mon projet professionnel, étant de devenir compétente à la fois en front et en back. Sur leur offre d'emploi il est indiqué qu'ils sont prêts à apprendre à leur futur employé les différents langages de programmation si tant est qu'il en maîtrise déjà au moins un. Ils m'ont dit que j'avais un profil intéressant et la RH m'a laissé sa carte avec son adresse mail et m'a dit de l'ajouter sur LinkedIn. Je l'ai ajouté et relancé aussi le vendredi soir pour l'informer du fait que la semaine prochaine était "banalisée" pour que l'on puisse passer des demi-journées au sein des entreprises qui nous intéressaient et qui étaient potentiellement intéressées par nous. 

Cette demi-journée a été riche en rencontres pour moi. J'ai vraiment aimé entrer en contact avec ces différentes personnes. L'appréhension que j'avais au début s'est vite effacée pour laisser place au plaisir d'échanger sur ce que j'aime faire et sur qui j'aimerais devenir professionnellement.

L'après-midi je suis restée au stand de l'IMTS. J'y ai fait la promotion de l'institut auprès de lycéens, dont certains au profil atypique, intéressés par le numérique et préparant leur avenir professionnel. 

Pour une première session, je suis plutôt satisfaite de ma journée même si nous sommes globalement plutôt d'accord sur le fait qu'une demi-journée aurait été suffisante. Cependant, maintenant que l'on sait à quel public nous avons à faire, l'année prochaine l'IMTS pourra peut-être prévoir différents ateliers en s'y prenant suffisamment tôt afin d'animer le stand sur la journée et qu'il y ait ainsi une activité continue.

