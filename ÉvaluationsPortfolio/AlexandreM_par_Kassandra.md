Lighthouse : 
- Performance : 54%
- Accessibility : 100%
- Best Practices : 100%
- SEO : 58%

W3C Validator​ : 

24 erreurs à corriger au niveau du code.

Test Responsive​:  
Pas très responsive au niveau des images et des titres.  Rien à redire pour les barres de compétences   

Contenu:  
Le texte n'est pas visible ce qui est dommage puisque le contenu est intéressant avec peut être un peu trop de redondances dans les termes employés, ce qui peut allourdir la lecture :
" Je suis passionné par le numérique ... Étant passioné par le numérique..." 

Quelques fautes d'orthographe "dévelLop(p)eur" par exemple. L'entête n'est pas visible non plus. Les couleurs sont sympathiques. Peut-être qu'une barre de navigation aurait pu faciliter la visite du site et la compréhension de son contenu. 

Peut-être créer une adresse e-mail un peu plus professionnelle ? 

Je placerais les compétences peut être un peu plus haut dans ton portfolio, puisque c'est ce qui compte le plus après tout. 

Enfin, peut-être ajouter une partie "portfolio", avec les travaux effectués depuis le début de l'année (screen des sites style fourmis solidaires...)

Point positif : 
Pas trop chargé en contenus, ce qui est une bonne chose visuellement parlant. 